﻿using UnityEngine;
using Infra.Utils;
using Ai.Goap;

namespace Army {
/// <summary>
/// Water jerican.
/// </summary>
[RequireComponent(typeof(SpriteRenderer))]
public class Jerican : PointOfInterest{
    public Sprite fullJerican;
    public Sprite emptyJerican;
    private int waterAmount = 4;
    private SpriteRenderer spriterenderer;
    private bool full = false;

    protected override void Awake() {
        base.Awake();
        spriterenderer = GetComponent<SpriteRenderer>();
        spriterenderer.sprite = emptyJerican;
        worldData["isFull"] = new StateValue(full);
    }

    public override State GetState() {
        base.GetState();
        worldData["isFull"].value = full;
        return worldData;
    }

    public void Fill(){
        waterAmount = 4;
        spriterenderer.sprite = fullJerican;
        full = true;
    }

    public void UseWater() {
        waterAmount -= 1;
        if (waterAmount == 0) {
            full = false;
            spriterenderer.sprite = emptyJerican;
            var father = GetComponentInParent<PointOfInterest>();
            if (father != null) {
                father.putEmptyJerican();
            }
        }
    }

    public bool isFull() {
        return full;
    }
}
}