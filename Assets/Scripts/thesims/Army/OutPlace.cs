﻿using UnityEngine;
using Infra.Utils;
using Ai.Goap;
using System.Collections;
using UnityEngine.UI;

namespace Army {
/// <summary>
/// Out place, for pawns leaving the camp.
/// </summary>
public class OutPlace : PointOfInterest{
    public GameObject outText;

    private SpriteRenderer spriteRenderer;
    private int numOfTrained;

    protected override void Awake() {
        base.Awake();
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = false;
        numOfTrained = 0;
    }

    public void portal(){
        StartCoroutine(disapeare());
    }

    public int getNumOfTrained(){
        return numOfTrained;
    }

    IEnumerator disapeare(){
        spriteRenderer.enabled = true;
        numOfTrained += 1;
        if (outText != null) {
            outText.GetComponent<Text>().text = "X " + numOfTrained;
        }
        yield return new WaitForSecondsRealtime(1.5f);
        spriteRenderer.enabled = false;
    }
}
}