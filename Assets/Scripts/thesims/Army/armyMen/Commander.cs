using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Ai.Goap;
using System.Collections;

namespace Army{
/// <summary>
/// Commander.
/// </summary>
public class Commander : Soldier{
    public SpriteRenderer exclamationMarkSprite;

    private List<IStateful> freePawns; //a list of the knowledge of the commander on witch pawn is not working, based on what they tell him.
    private readonly WorldGoal worldGoal = new WorldGoal();
    private bool askedPawnIsFree = false;

    protected override void Awake(){
        base.Awake();
        freePawns = new List<IStateful>();
        ResetPawnsList();
        exclamationMarkSprite.enabled = false;

        var goal = new Goal();
        goal["havingTrip"] = new Condition(CompareType.Equal, true);
        worldGoal[this] = goal;
    }

    public override WorldGoal CreateGoalState(){
        return worldGoal;
    }

    public void PawnIsBusy(IStateful pawn){
        freePawns.Remove(pawn);
        PlanFailed(worldGoal);    //this can happen only whene the commander give an order and the pawn is busy.
    }

    public override void PlanFailed(WorldGoal failedGoal) {
        base.PlanFailed(failedGoal);
        AddHappiness(-0.1f);
        GoalFinished();
    }

    public void ResetPawnsList(){   //whene a pawn is leaving and another one comes.
        freePawns.Clear();
        var temp = GameObject.FindGameObjectsWithTag("Pawns");
        foreach(var pawn in temp) {
            freePawns.Add(pawn.GetComponent<Pown>());
        }
    }

    public override State GetState() {
        var state = base.GetState();
        state["askPawnIsFree"] = new StateValue(askedPawnIsFree);
        state["atTent"] = new StateValue(false);

        return state;
    }

    public void GetCommandFromUser(string command, object val){
        for (int i = 1; i <= 3; i++) {
            availableActions.ElementAt(i).GetComponent<GivePawnCommand>().changeTargets(freePawns);
        }
        StartCoroutine(getOrderAsTarget(1, command, val));

        ResetPawnsList();
    }

    public List<IStateful> GetFreePawns(){
        return freePawns;
    }

    public void GoalFinished(){
        var goal = new Goal();
        if (hunger > 0.75f && GameObject.FindObjectOfType<Table>().getNumOfMeals() > 0) {
            goal["eat"] = new Condition(CompareType.Equal, true);
        } else {
            goal["havingTrip"] = new Condition(CompareType.Equal, true);
        }
        worldGoal[this] = goal;
    }

    public void didIAskedPawn(bool asked) {
        askedPawnIsFree = asked;
    }

    IEnumerator getOrderAsTarget(int seconds, string command, object val){
        exclamationMarkSprite.enabled = true;
        yield return new WaitForSecondsRealtime(seconds);
        exclamationMarkSprite.enabled = false;

        var goal = new Goal();
        goal[command] = new Condition(CompareType.Equal, val);
        worldGoal[this] = goal;
    }
}
}