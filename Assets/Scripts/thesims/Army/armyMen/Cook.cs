﻿using UnityEngine;
using System.Collections.Generic;
using Infra.Utils;
using Ai.AStar;
using Ai.Goap;

namespace Army{
/// <summary>
/// The Cook.
/// </summary>
public class Cook : Soldier{
    private readonly WorldGoal worldGoal = new WorldGoal();

    protected override void Awake(){
        base.Awake();

        var goal = new Goal();
        goal["makeFood"] = new Condition(CompareType.Equal, true);
        worldGoal[this] = goal;
    }

    public override State GetState() {
        var state = base.GetState();
        state["makeFood"] = new StateValue(false);
        state["atTent"] = new StateValue(false);

        return state;
    }

    public override WorldGoal CreateGoalState(){
        var goal = new Goal();
        var table = GameObject.FindObjectOfType<Table>();
        if (hunger > 0.75 || happiness < 0.2f) {
            goal["eat"] = new Condition(CompareType.Equal, true);
        } else if (table.getNumOfBoxes() > 0 && table.getNumOfMeals() == 0 && table.isClean()){
            goal["makeFood"] = new Condition(CompareType.Equal, true);
        } else {
            goal["havingTrip"] = new Condition(CompareType.Equal, true);
        }
        worldGoal[this] = goal;
        return worldGoal;
    }
}
}