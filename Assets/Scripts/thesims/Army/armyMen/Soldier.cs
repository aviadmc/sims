﻿using UnityEngine;
using System.Collections.Generic;
using Infra.Utils;
using Ai.AStar;
using Ai.Goap;

namespace Army{
/// <summary>
/// Soldier.
/// </summary>
[RequireComponent(typeof(Hands))]
public abstract class Soldier : GoapAgent {
    [SerializeField] Sprite mouthHappy;
    [SerializeField] Sprite mouthSad;
    [SerializeField] Sprite mouthAngry;
    [SerializeField] Sprite mouthNothing;
    [SerializeField] float moveSpeed = 1;

    protected Hands hands;
    private readonly State state = new State();
    private SpriteRenderer mouthSpriterendere;
    protected float happiness = 0.5f;  //numbet in range [0,1]
    protected float anger = 0f;   //numbet in range [0,1]
    protected float hunger = 0f;  //numbet in range [0,1]
    private bool didIJustFilledJerican;

    protected override void Awake() {
        base.Awake();
        mouthSpriterendere = GetComponentsInChildren<SpriteRenderer>()[1];
        hands = GetComponent<Hands>();
        didIJustFilledJerican = false;
        state["neerTent"] = new StateValue(false);
        state["neerTable"] = new StateValue(false);
        state["neerFaucet"] = new StateValue(false);
        state["JericanFull"] = new StateValue(didIJustFilledJerican);
        state["handsFull"] = new StateValue(false);
        state["hasEmptyJerican"] = new StateValue(false);
        state["hasFullJerican"] = new StateValue(false);
    }

    protected void Start() {
        // faces:
        if (mouthHappy == null) {
            mouthHappy = gameObject.GetComponent<Sprite>();
        }
        if (mouthSad == null) {
            mouthSad = gameObject.GetComponent<Sprite>();
        }
        if (mouthAngry == null) {
            mouthAngry = gameObject.GetComponent<Sprite>();
        }
        if (mouthNothing == null) {
            mouthNothing = gameObject.GetComponent<Sprite>();
        }
        ChangeMouth();
    }

    public void AddHappiness(float toAdd){
        happiness = Mathf.Max(0, Mathf.Min(1, happiness + toAdd));
        ChangeMouth();
    }

    public void AddAnger(float toAdd){
        anger = Mathf.Max(0, Mathf.Min(1, anger + toAdd));
    }

    public void AddHunger(float toAdd) {
        hunger = Mathf.Max(0, Mathf.Min(1, hunger + toAdd));
    }

    protected void ChangeMouth() {
        Sprite cur;
        if (anger > 0.8f) {
            cur = mouthAngry;
        }else if(happiness < 0.33f) {
            cur = mouthSad;
        }else if(happiness < 0.66f) {
            cur = mouthNothing;
        }else {
            cur = mouthHappy;
        }
        mouthSpriterendere.sprite = cur;
    }

    public override WorldGoal CreateGoalState(){
        return new WorldGoal();
    }

    public override State GetState() {
        Jerican holdedJerican = GetComponentInChildren<Jerican>();
        state["neerTent"].value = Vector2.Distance((Vector2)transform.position, (Vector2)(GameObject.Find("Tent").transform.position)) < 3;
        state["neerTable"].value = Vector2.Distance((Vector2)transform.position, (Vector2)(GameObject.Find("Table").transform.position)) < 3;
        state["neerFaucet"].value = Vector2.Distance((Vector2)transform.position, (Vector2)(GameObject.Find("Faucet").transform.position)) < 3;
        state["hasEmptyJerican"].value = holdedJerican != null && !holdedJerican.isFull();
        state["hasFullJerican"].value = holdedJerican != null && holdedJerican.isFull();
        state["x"] = new StateValue(transform.position.x);
        state["y"] = new StateValue(transform.position.y);
        state["handsFull"].value = hands.holdedItem == null;
        state["JericanFull"].value = didIJustFilledJerican;

        return state;
    }

    public void justFillJerican(bool didI){
        didIJustFilledJerican = didI;
    }

    public override void PlanFailed(WorldGoal failedGoal) {
        // If this happens for too long, there is probably a bug in the actions,
        // goals or world setup.
        // TODO: Make sure the world state has changed before running the same
        //       goal again.
        // TODO: Support multiple goals and select the next one.

        Debug.Log("Plan Failed");
    }

    public override void PlanFound(WorldGoal goal, Queue<ITransition> actions) {
        // Yay we found a plan for our goal!
        Debug.Log("<color=green>Plan found</color> " + GoapAgent.PrettyPrint(actions));
    }

    public override void AboutToDoAction(GoapAction.WithContext action) {
        var talkBubble = GetComponentInChildren<Talk>();
        if (talkBubble != null) {
            var sprite = GameObject.Find(action.ToString().Split(new char[] { '@' })[1]).GetComponentInChildren<SpriteRenderer>().sprite;
            talkBubble.say(sprite);
        }
    }

    public override void ActionsFinished() {
        // Everything is done, we completed our actions for this gool. Hooray!
        Debug.Log("<color=blue>Actions completed</color>");
        var talkBubble = GetComponentInChildren<Talk>();
        if (talkBubble != null) {
            talkBubble.say(null);
        }
    }

    public override void PlanAborted(GoapAction.WithContext aborter) {
        // An action bailed out of the plan. State has been reset to plan again.
        // Take note of what happened and make sure if you run the same goal
        // again that it can succeed.
        Debug.Log("<color=red>Plan Aborted</color> " + aborter);
        //our TODO: add here staff
    }

    public override bool MoveAgent(GoapAction.WithContext nextAction) {
        // Move towards the NextAction's target.
        float step = moveSpeed * Time.deltaTime;
        var target = nextAction.target as Component;
        // NOTE: We must cast to Vector2, otherwise we'll compare the Z coordinate
        //       which does not have to match!
        var position = (Vector2)target.transform.position;
        // TODO: Move by setting the velocity of a rigid body to allow collisions.
        transform.position = Vector2.MoveTowards(transform.position, position, step);

        if (position.Approximately(transform.position)) {
            // We are at the target location, we are done.
            nextAction.isInRange = true;
            return true;
        }
        return false;
    }
}
}