﻿using UnityEngine;
using Ai.Goap;

namespace Army{
public class Pown : Soldier{
    public Sprite alternativeSprite;

    private readonly WorldGoal worldGoal = new WorldGoal();
    private int xp;
    private SpriteRenderer spriteRenderer;
    private Transform pawnTransform;
    private bool isInTent;
    private bool obeyingOrders;
    private XpBar xpBar;
    private int numBoxesDelivered;
    private bool isFree;

    protected override void Awake(){
        base.Awake();
        xp = 1;
        spriteRenderer = GetComponent<SpriteRenderer>();
        pawnTransform = GetComponent<Transform>();
        xpBar = GetComponentInChildren<XpBar>();
        isFree = true;
        isInTent = false;
        obeyingOrders = false;
        numBoxesDelivered = 0;


        xpBar.curXP(xp);
    }

    //a "new" pawn
    public void reBirth(){
        base.Awake();
        xp = 1;
        isInTent = false;
        obeyingOrders = false;

        xpBar.curXP(xp);
    }

    public override WorldGoal CreateGoalState(){
        if (xp == 100) {
            var goal = new Goal();
            goal["goToTheArmy"] = new Condition(CompareType.Equal, true);
            worldGoal[this] = goal;
        } else if (!obeyingOrders) {
            var goal = new Goal();
            if ((hunger > 0.75 || (hunger - happiness > 0)) && GameObject.FindObjectOfType<Table>().getNumOfMeals() > 0) {
                goal["eat"] = new Condition(CompareType.Equal, true);
            } else {
                float f = Random.value;
                if (f > 0.5f) {
                    goal["sleep"] = new Condition(CompareType.Equal, true);
                } else {
                    goal["havingTrip"] = new Condition(CompareType.Equal, true);
                }
            }
            worldGoal[this] = goal;
        }
        return worldGoal;
    }


    public bool ObeyOrRefuse() {
        if (obeyingOrders) {
            isFree = false;
            return isFree;
        }
        float chance = (1 - anger) * happiness;
        isFree = chance >= 0.5f - Random.value;
        return isFree;
    }

    public override State GetState() {
        var state = base.GetState();
        state["atTent"] = new StateValue(isInTent);
        state["boxesDelivered"] = new StateValue(numBoxesDelivered);
        state["hasBoxWithFood"] = new StateValue(hands.holdedItem != null && hands.holdedItem.ToString().Contains("FoodBox"));
        state["pawnIsFree"] = new StateValue(isFree);

        return state;
    }

    public void GetOrder(string goalName, object obj){
        if (isInTent) {
            sleepAndWakeUp();
        }
        obeyingOrders = true;

        var goal = new Goal();
        goal[goalName] = new Condition(CompareType.Equal, obj);
        worldGoal[this] = goal;
    }

    public override void ActionsFinished(){
        base.ActionsFinished();
        obeyingOrders = false;
    }

    public override void PlanFailed(WorldGoal failedGoal) {
        base.PlanFailed(failedGoal);
        AddHappiness(-0.1f);

        obeyingOrders = false;
    }

    public void addXp(int toAdd){
        xp += toAdd;
        xp = Mathf.Max(xp, 1);
        xp = Mathf.Min(xp, 100);
        xpBar.curXP(xp);
    }

    public void sleepAndWakeUp(){
        isInTent = !isInTent;
        Sprite sprite = alternativeSprite;
        alternativeSprite = spriteRenderer.sprite;
        spriteRenderer.sprite = sprite;
        pawnTransform.GetChild(0).gameObject.SetActive(!isInTent);
    }

    public void deliverBox(){
        numBoxesDelivered += 1;
        if (numBoxesDelivered == 10) {
            numBoxesDelivered = 0;
        }
    }
}
}