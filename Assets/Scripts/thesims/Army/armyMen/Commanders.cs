﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Army {
/// <summary>
/// Commanders.
/// </summary>
public class Commanders : MonoBehaviour {
    private Commander[] allCommanders;
    private int commanderInd = 0;

    private void Start(){
        allCommanders = gameObject.GetComponentsInChildren<Commander>();
    }

    public void commandFromUser(string command){
        Commander curCommander = allCommanders[commanderInd];
        commanderInd += 1;
        if (commanderInd == allCommanders.Length) {
            commanderInd = 0;
        }
        if (command == "boxesDelivered") {
            curCommander.GetCommandFromUser(command, 10);
        } else {
            curCommander.GetCommandFromUser(command, true);
        }
    }
}
}