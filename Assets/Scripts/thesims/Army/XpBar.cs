﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Army {
/// <summary>
/// Xp bar, for pawns.
/// </summary>
public class XpBar : MonoBehaviour {
    private float maxCenter = 0.04f;
    private float minCenter = -0.4f;
    private float maxXsize = 1.536227f;
    private float minXsize = 0.1536227f;
    private Transform xpTransform;

    public void curXP(int curxp){
        xpTransform = GetComponent<Transform>();
        float curXcenter = (((maxCenter - minCenter)/100f) * curxp) + minCenter;
        float curXsize = (((maxXsize - minXsize)/100f) * curxp) + minXsize;
        xpTransform.localPosition = new Vector3(curXcenter, xpTransform.localPosition.y, 0);
        xpTransform.localScale = new Vector3(curXsize, xpTransform.localScale.y, 0);
    }
}
}