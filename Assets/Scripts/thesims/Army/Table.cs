﻿using UnityEngine;
using Infra.Utils;
using Ai.Goap;

namespace Army {
/// <summary>
/// Food Table.
/// </summary>
public class Table : PointOfInterest{
    public Sprite emptyTable;
    public Sprite fullTable;
    public Sprite dirtyTable;

    private int numOfRation;
    private int numOfBoxes;
    private SpriteRenderer spriteRenderer;
    private bool isDirty;

    protected override void Awake() {
        base.Awake();

        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = emptyTable;
        isDirty = false;
        numOfRation = 0;
        worldData["numMeals"] = new StateValue(numOfRation);
        worldData["isClean"] = new StateValue(!isDirty);
        numOfBoxes = 0;
        worldData["BoxesNearTheTable"] = new StateValue(numOfBoxes);
    }

    public override State GetState() {
        base.GetState();
        worldData["numMeals"].value = numOfRation;
        worldData["isClean"].value = (!isDirty);
        worldData["BoxesNearTheTable"].value = numOfBoxes;
        return worldData;
    }

    public void makeMeal(int numOfMeals){
        numOfBoxes -= numOfMeals;
        numOfRation += numOfMeals;
        spriteRenderer.sprite = fullTable;
    }

    public bool isClean(){
        return !isDirty;
    }

    public bool eat(){
        if (numOfRation > 0) {
            numOfRation -= 1;
            if (numOfRation == 0) {
                isDirty = true;
                spriteRenderer.sprite = dirtyTable;
            }
            return true;
        }
        return false;
    }

    public void cleanTable(){
        isDirty = false;
        spriteRenderer.sprite = emptyTable;
    }

    public void putDownBox(){
        numOfBoxes++;
    }

    public int getNumOfBoxes(){
        return numOfBoxes;
    }

    public int getNumOfMeals() {
        return numOfRation;
    }
}
}