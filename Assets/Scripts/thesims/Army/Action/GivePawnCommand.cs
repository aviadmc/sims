﻿using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace Army {
/// <summary>
/// Give pawn command, given by the player.
/// </summary>
public class GivePawnCommand : GoapAction {
    private List<IStateful> targets;
    public string command;
    public ModificationType type;
    public bool isBool;
    public int value;

    protected void Awake() {
        AddPrecondition("askPawnIsFree", CompareType.Equal, true);  //not working well :(
        if (isBool) {
            AddEffect(command, type, value != 0);
        } else {
            AddEffect(command, ModificationType.Set, value);
        }
        AddEffect("askPawnIsFree", ModificationType.Set, false);
    }

    protected void Start() {
        targets = GetTargets<Pown>();
    }

    public void changeTargets(List<IStateful> tar){
        targets = tar;
    }

    public override bool RequiresInRange() {
        return true;
    }

    public override List<IStateful> GetAllTargets(GoapAgent agent) {
        return targets;
    }

    protected override bool OnDone(GoapAgent agent, WithContext context) {
        var target = context.target as Component;

        if (isBool) {
            target.GetComponent<Pown>().GetOrder(command, value != 0);
        } else {
            target.GetComponent<Pown>().GetOrder(command, value);
        }

        agent.GetComponent<Soldier>().AddHunger(0.15f);
        agent.GetComponent<Commander>().GoalFinished();

        return base.OnDone(agent, context);
    }
}
}