﻿using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace Army {
//we made this because we can't make an action with more than one object.
public class FillJericanHelper : GoapAction {
    private List<IStateful> targets;

    protected void Awake() {
        AddPrecondition("JericanFull", CompareType.Equal, true);
        AddTargetEffect("isFull", ModificationType.Set, true);
        AddEffect("makeFood", ModificationType.Set, true);
    }

    protected void Start() {
        targets = GetTargets<Jerican>();
    }

    public override bool RequiresInRange() {
        return false;   //well, it's a kind of bug here... if it's "true" the soldier might go to a jerican that not at the faucet.
    }

    public override List<IStateful> GetAllTargets(GoapAgent agent) {
        return targets;
    }

    protected override bool OnDone(GoapAgent agent, WithContext context) {
        agent.GetComponent<Soldier>().justFillJerican(false);
        return base.OnDone(agent, context);
    }
}
}