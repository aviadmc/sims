﻿using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace Army {
/// <summary>
/// Eat.
/// </summary>
public class eat : GoapAction {
    private List<IStateful> targets;

    protected void Awake() {
        AddPrecondition("atTent", CompareType.Equal, false);
        AddTargetPrecondition("numMeals", CompareType.MoreThan, 0);
        AddEffect("eat", ModificationType.Set, true);
    }

    protected void Start() {
        targets = GetTargets<Table>();
    }

    public override bool RequiresInRange() {
        return true;
    }

    public override List<IStateful> GetAllTargets(GoapAgent agent) {
        return targets;
    }

    protected override bool OnDone(GoapAgent agent, WithContext context) {
        var target = context.target as Component;
        var table = target.gameObject.GetComponent<Table>();
        table.eat();

        agent.GetComponent<Soldier>().AddHunger(-1f);
        agent.GetComponent<Soldier>().AddHappiness (0.4f);
        var pawn = agent.GetComponent<Pown>();
        if (pawn != null) pawn.addXp(-3);


        return base.OnDone(agent, context);
    }
}
}