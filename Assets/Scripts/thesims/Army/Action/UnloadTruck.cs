﻿using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace Army {
/// <summary>
/// taking creates from the truck.
/// </summary>
public class UnloadTruck : GoapAction {

    private List<IStateful> targets;

    protected void Awake() {
        AddTargetPrecondition("InPosition", CompareType.Equal, true);
        AddTargetPrecondition("NumOfBoxes", CompareType.MoreThan, 0);
        AddEffect("hasBoxWithFood", ModificationType.Set, true);
        AddTargetEffect("NumOfBoxes", ModificationType.Add, -1);
    }

    protected void Start() {
        targets = GetTargets<Truck>();
    }

    public override bool RequiresInRange() {
        return true;
    }

    public override List<IStateful> GetAllTargets(GoapAgent agent) {
        return targets;
    }

    protected override bool OnDone(GoapAgent agent, WithContext context) {
        var target = context.target as Component;
        var hands = agent.GetComponent<Hands>();
        var prefab = target.GetComponent<Truck>().unloadBox();
        var box = Instantiate(prefab, agent.transform.position, agent.transform.rotation);
        hands.holdedItem = box;
        box.transform.parent = agent.transform;

        return base.OnDone(agent, context);
    }
}
}
