using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace Army {
/// <summary>
/// goint to the targets for trainig. How fun!
/// </summary>
public class TrainingShooting : GoapAction {
    private List<IStateful> targets;

    protected void Awake() {
        AddEffect("gotTrained", ModificationType.Set, true);
    }

    protected void Start() {
        targets = GetTargets<ShootingTargets>();
    }

    public override bool RequiresInRange() {
        return true;
    }

    public override List<IStateful> GetAllTargets(GoapAgent agent) {
        return targets;
    }

    protected override bool OnDone(GoapAgent agent, WithContext context) {
        agent.GetComponent<Pown>().addXp(20);       //he trained
        agent.GetComponent<Soldier>().AddHunger(0.25f);
        agent.GetComponent<Pown>().AddHappiness(-0.1f);
        if (Random.Range (1, 5) == 4)
            agent.GetComponent<Pown> ().AddAnger (0.2f);

        if (Random.Range (1, 10) == 7) {
            agent.GetComponent<Pown> ().addXp (30);
        }
        return base.OnDone(agent, context);
    }
}
}
