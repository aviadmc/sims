﻿using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace Army {
/// <summary>
/// goint to sleep at the tent.
/// </summary>
public class GoToSleep : GoapAction {
    private List<IStateful> targets;

    protected void Awake() {
        AddPrecondition("atTent", CompareType.Equal, false);
        AddEffect("atTent", ModificationType.Set, true);
    }

    protected void Start() {
        targets = GetTargets<Tent>();
    }

    public override bool RequiresInRange() {
        return true;
    }

    public override List<IStateful> GetAllTargets(GoapAgent agent) {
        return targets;
    }

    protected override bool OnDone(GoapAgent agent, WithContext context) {
        agent.GetComponent<Pown>().sleepAndWakeUp();
        agent.GetComponent<Soldier>().AddHunger(0.1f);
        return base.OnDone(agent, context);
    }
}
}
