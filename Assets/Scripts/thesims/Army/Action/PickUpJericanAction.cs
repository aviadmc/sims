using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace Army {
/// <summary>
/// Pick up a jerican.
/// </summary>
public class PickUpJericanAction : GoapAction {
    private List<IStateful> targets;
    public bool isFull;

    protected void Awake() {
        AddPrecondition("atTent", CompareType.Equal, false);
        AddTargetPrecondition("isFull", CompareType.Equal, isFull);
        AddEffect("has" + (isFull ? "Full" : "Empty") + "Jerican", ModificationType.Set, true);
    }

    protected void Start() {
        targets = GetTargets<Jerican>();
    }

    public override bool RequiresInRange() {
        return true;
    }

    public override List<IStateful> GetAllTargets(GoapAgent agent) {
        return targets;
    }

    protected override bool OnDone(GoapAgent agent, WithContext context) {
        var target = context.target as Component;
        target.gameObject.transform.parent = agent.transform;

        var hands = agent.GetComponent<Hands>();
        hands.holdedItem = target.gameObject;

        return base.OnDone(agent, context);
    }
}
}
