﻿using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace Army {
public class UseWater : GoapAction {
    private List<IStateful> targets;

    protected void Awake() {
		AddPrecondition("hasFullJerican", CompareType.Equal, true);
		AddEffect("UseWater", ModificationType.Set, true);
    }

    protected void Start() {
        targets = GetTargets<Jerican>();
    }

    public override bool RequiresInRange() {
        return true;
    }

    public override List<IStateful> GetAllTargets(GoapAgent agent) {
        return targets;
    }

	protected override bool OnDone(GoapAgent agent, WithContext context) {
		var target = context.target as Component;
		((Jerican)target).UseWater();

		return base.OnDone(agent, context);
	}
}
}