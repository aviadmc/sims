﻿using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace Army {
/// <summary>
/// wake up!
/// </summary>
public class WakeUp : GoapAction {
    private List<IStateful> targets;

    protected void Awake() {
        AddPrecondition("sleep", CompareType.Equal, true);
        AddEffect("atTent", ModificationType.Set, false);
    }

    protected void Start() {
        targets = GetTargets<Tent>();
    }

    public override bool RequiresInRange() {
        return true;
    }

    public override List<IStateful> GetAllTargets(GoapAgent agent) {
        return targets;
    }

    protected override bool OnDone(GoapAgent agent, WithContext context) {
        Pown pawn = agent.GetComponent<Pown>();
        pawn.sleepAndWakeUp();

        return base.OnDone(agent, context);
    }
}
}
