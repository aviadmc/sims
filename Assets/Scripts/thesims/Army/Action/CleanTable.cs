﻿using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace Army {
/// <summary>
/// Clean table, so the cook can make more food.
/// </summary>
public class CleanTable : GoapAction {
    private List<IStateful> targets;

    protected void Awake() {
        AddTargetPrecondition("WithFullJerican", CompareType.Equal, true);
        AddTargetPrecondition("isClean", CompareType.Equal, false);
        AddTargetEffect("isClean", ModificationType.Set, true);
        AddEffect("TableCleaned", ModificationType.Set, true);
    }

    protected void Start() {
        targets = GetTargets<Table>();
    }

    public override bool RequiresInRange() {
        return true;
    }

    public override List<IStateful> GetAllTargets(GoapAgent agent) {
        return targets;
    }

    protected override bool OnDone(GoapAgent agent, WithContext context) {
        var target = context.target as Component;
        target.GetComponent<Table>().cleanTable();
        target.GetComponentInChildren<Jerican>().UseWater();
        agent.GetComponent<Pown>().addXp(5);
        agent.GetComponent<Pown>().AddHappiness (-0.3f);
        agent.GetComponent<Pown>().AddAnger (0.1f);
        agent.GetComponent<Soldier>().AddHunger(0.05f);


        return base.OnDone(agent, context);
    }
}
}