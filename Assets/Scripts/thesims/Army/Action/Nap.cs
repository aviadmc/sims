﻿using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace Army {
/// <summary>
/// goint to sleep at the tent.
/// </summary>
public class Nap : GoapAction {
    private List<IStateful> targets;

    protected void Awake() {
        AddPrecondition("atTent", CompareType.Equal, true);
        AddEffect("sleep", ModificationType.Set, true);
    }

    protected void Start() {
        targets = GetTargets<Tent>();
    }

    public override bool RequiresInRange() {
        return true;
    }

    public override List<IStateful> GetAllTargets(GoapAgent agent) {
        return targets;
    }

    protected override bool OnDone(GoapAgent agent, WithContext context) {
        Pown pawn = agent.GetComponent<Pown>();
        pawn.addXp(-2);
        pawn.AddHappiness(0.05f);
        pawn.AddAnger(-0.2f);

        return base.OnDone(agent, context);
    }
}
}
