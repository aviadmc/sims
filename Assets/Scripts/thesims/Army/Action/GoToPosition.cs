﻿using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace Army {
/// <summary>
/// Go to position, for a little trip.
/// </summary>
public class GoToPosition : GoapAction {
    public PointOfInterestType place;

    private List<IStateful> targets;

    protected void Awake() {
        AddPrecondition("neer" + place, CompareType.Equal, false);
        AddPrecondition("atTent", CompareType.Equal, false);
        AddEffect("havingTrip", ModificationType.Set, true);
        cost = Random.Range(0.5f, 4.5f);
    }

    protected void Start() {
        targets = PointOfInterest.GetPointOfInterest(place);
    }

    public override bool RequiresInRange() {
        return true;
    }

    public override List<IStateful> GetAllTargets(GoapAgent agent) {
        return targets;
    }

    protected override bool OnDone(GoapAgent agent, WithContext context) {
        cost = Random.Range(0.5f, 4.5f);    //to make randomness in the trips
        return base.OnDone(agent, context);
    }
}
}