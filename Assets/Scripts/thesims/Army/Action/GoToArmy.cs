﻿using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace Army {
/// <summary>
/// goint to the real army!
/// </summary>
public class GoToArmy : GoapAction {
    private List<IStateful> targets;

    protected void Awake() {
        AddEffect("goToTheArmy", ModificationType.Set, true);
    }

    protected void Start() {
        targets = GetTargets<OutPlace>();
    }

    public override bool RequiresInRange() {
        return true;
    }

    public override List<IStateful> GetAllTargets(GoapAgent agent) {
        return targets;
    }

    protected override bool OnDone(GoapAgent agent, WithContext context) {
        var target = context.target as Component;
        target.GetComponent<OutPlace>().portal();
        var pawn = agent.GetComponent<Pown>();
        pawn.reBirth();
        agent.GetComponent<Transform>().position = new Vector3(-10, 0, 0);

        return base.OnDone(agent, context);
    }
}
}
