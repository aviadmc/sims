﻿using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace Army {
/// <summary>
/// Put down a jerican.
/// </summary>
public class PutDownJerican : GoapAction {
    public PointOfInterest place;
    private List<IStateful> targets;
    public bool isFull;

    protected void Awake() {
        AddPrecondition("atTent", CompareType.Equal, false);
        AddPrecondition("has" + (isFull ? "Full" : "Empty") + "Jerican", CompareType.Equal, true);
        AddTargetPrecondition("WithFullJerican", CompareType.Equal, false);
        AddTargetPrecondition("WithEmptyJerican", CompareType.Equal, false);
        AddEffect("has" + (isFull ? "Full" : "Empty") + "Jerican", ModificationType.Set, false);
        AddTargetEffect("With" + (isFull ? "Full" : "Empty") + "Jerican", ModificationType.Set, true);
        AddEffect((isFull ? "Full" : "Empty") + "JericanAt" + place, ModificationType.Set, true);
    }

    protected void Start() {
        targets = PointOfInterest.GetPointOfInterest(place.type);
    }

    public override bool RequiresInRange() {
        return true;
    }

    public override List<IStateful> GetAllTargets(GoapAgent agent) {
        return targets;
    }

    protected override bool OnDone(GoapAgent agent, WithContext context) {
        var target = context.target as Component;
        var point = target.gameObject.GetComponent<PointOfInterest>();
        if (point != null) {
            if (isFull) {
                point.putFullJerican();
            } else {
                point.putEmptyJerican();
            }
        }

        agent.GetComponent<Hands>().holdedItem.transform.parent = target.transform;
        agent.GetComponent<Hands>().holdedItem = null;

        return base.OnDone(agent, context);
    }
}
}

