﻿using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace Army {
/// <summary>
/// make food for the hungry soldiers!
/// </summary>
public class MakeFood : GoapAction {
    private List<IStateful> targets;

    protected void Awake() {
        AddTargetPrecondition("isClean", CompareType.Equal, true);
        AddTargetPrecondition("numMeals", CompareType.Equal, 0);
        AddTargetPrecondition("BoxesNearTheTable", CompareType.MoreThan, 0);
        AddEffect("makeFood", ModificationType.Set, true);
    }

    protected void Start() {
        targets = GetTargets<Table>();
    }

    public override bool RequiresInRange() {
        return true;
    }

    public override List<IStateful> GetAllTargets(GoapAgent agent) {
        return targets;
    }

    protected override bool OnDone(GoapAgent agent, WithContext context) {
        var target = context.target as Component;
        var table = target.GetComponent<Table>();

        int counter = table.transform.childCount;
        for (int i = 0; i < table.transform.childCount; i++) {
            var curBox = table.transform.GetChild(i);
            if (curBox.ToString().Contains("FoodBox")) {
                curBox.gameObject.SetActive(false); //all the boxes are not needed now
            }
        }
        table.transform.DetachChildren();

        table.makeMeal(table.getNumOfBoxes());

        return base.OnDone(agent, context);
    }
}
}
