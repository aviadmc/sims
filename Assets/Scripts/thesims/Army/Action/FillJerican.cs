﻿using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace Army {
/// <summary>
/// Fill jerican with water.
/// </summary>
public class FillJerican : GoapAction {
    private List<IStateful> targets;

    protected void Awake() {
        AddTargetPrecondition("WithEmptyJerican", CompareType.Equal, true);
        AddEffect("JericanFull", ModificationType.Set, true);
    }

    protected void Start() {
        targets = GetTargets<Faucet>();
    }

    public override bool RequiresInRange() {
        return true;
    }

    public override List<IStateful> GetAllTargets(GoapAgent agent) {
        return targets;
    }

    protected override bool OnDone(GoapAgent agent, WithContext context) {
        var target = context.target as Component;
        var soldier = agent.GetComponent<Soldier>();
            soldier.AddHunger(0.1f);
        soldier.justFillJerican(true);
            target.GetComponentInChildren<Jerican>().Fill();
		
        return base.OnDone(agent, context);
    }
}
}