﻿using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

/// <summary>
/// Put box on table, for making food.
/// </summary>
namespace Army {
public class PutBoxOnTable : GoapAction {
    private List<IStateful> targets;

    protected void Awake() {
        AddPrecondition("hasBoxWithFood", CompareType.Equal, true);
        AddTargetEffect("BoxesNearTheTable", ModificationType.Add, 1);
        AddEffect("boxesDelivered", ModificationType.Set, 10);  //for some reasons, "Add" does not working :(
    }

    protected void Start() {
        targets = GetTargets<Table>();
    }

    public override bool RequiresInRange() {
        return true;
    }

    public override List<IStateful> GetAllTargets(GoapAgent agent) {
        return targets;
    }

    protected override bool OnDone(GoapAgent agent, WithContext context) {
        var target = context.target as Component;
        var table = target.gameObject.GetComponent<Table>();
        table.putDownBox();

        agent.GetComponent<Hands>().holdedItem.transform.parent = table.transform;
        agent.GetComponent<Hands>().holdedItem = null;
        agent.GetComponent<Pown>().deliverBox();
        agent.GetComponent<Pown>().AddHappiness (-0.05f);
        agent.GetComponent<Pown>().addXp (3);
        agent.GetComponent<Soldier>().AddHunger(0.3f);
        return base.OnDone(agent, context);
    }
}
}