﻿using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace Army {
public class AskPawnIfFree : GoapAction {
    private List<IStateful> targets;

    protected void Awake() {
        AddEffect("askPawnIsFree", ModificationType.Set, true);
    }

    protected void Start() {
        targets = GetTargets<Pown>();
    }

    public override bool RequiresInRange() {
        return true;
    }

    public override List<IStateful> GetAllTargets(GoapAgent agent) {
        return targets;
    }

    protected override bool OnDone(GoapAgent agent, WithContext context) {
        var target = context.target as Component;
        agent.GetComponent<Soldier>().AddHunger(0.05f);
        var pawn = target.gameObject.GetComponent<Pown>();
        if (!pawn.ObeyOrRefuse()) {
            agent.GetComponent<Commander>().PawnIsBusy(target.gameObject.GetComponent<Pown>());
        }
        agent.GetComponent<Commander>().didIAskedPawn(true);

        return base.OnDone(agent, context);
    }
}
}