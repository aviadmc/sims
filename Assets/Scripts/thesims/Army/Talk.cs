﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Army {
/// <summary>
/// Talk. You know, for saying things.
/// </summary>
public class Talk : MonoBehaviour {

    private SpriteRenderer spriteRenderer;

    // Use this for initialization
    void start() {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void say(Sprite sprite){
        if (spriteRenderer == null) {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }
        if (sprite != null) {
            spriteRenderer.enabled = true;
            GetComponentsInChildren<SpriteRenderer>()[1].sprite = sprite;
        } else {
            GetComponentsInChildren<SpriteRenderer>()[1].sprite = null;
            spriteRenderer.enabled = false;
        }
    }
}
}