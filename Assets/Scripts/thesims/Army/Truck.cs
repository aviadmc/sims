﻿using System.Collections;
using UnityEngine;
using Infra.Utils;
using Ai.Goap;

namespace Army {
/// <summary>
/// Truck, bringing food
/// </summary>
public class Truck : PointOfInterest{
    public GameObject boxPrefab;
    private Transform truckTransform;
    private int numOfMeals;
    private bool toDrive;    //should I put gas?

    private float midCamp = 0f;

    protected override void Awake() {
        base.Awake();
        toDrive = false;
        truckTransform = GetComponent<Transform>();
        comeToCamp();
        worldData["InPosition"] = new StateValue(false);
        worldData["NumOfBoxes"] = new StateValue(numOfMeals);
    }

    // Update is called once per frame
    void Update () {
        if (toDrive && transform.position.x > -9.5) {
            truckTransform.Translate(Vector2.left * Time.deltaTime * 2);
            if (Mathf.Abs(transform.position.x - midCamp) < 0.025f) {
                StartCoroutine(wait(3 + 3 * numOfMeals));
            }
        } else if (transform.position.x <= -9.5){
            comeToCamp();
        }
    }

    public override State GetState() {
        base.GetState();
        worldData["InPosition"].value = Mathf.Abs(transform.position.x - midCamp) < 0.02f;
        worldData["NumOfBoxes"].value = numOfMeals;
        return worldData;
    }

    private void comeToCamp(){
        transform.position = new Vector2(10, transform.position.y);
        numOfMeals = Random.Range(3, 8);
        int time = Random.Range(10, 20);
        StartCoroutine(wait(time));
    }

    IEnumerator wait(int seconds){
        toDrive = false;
        yield return new WaitForSecondsRealtime(seconds);
        toDrive = true;
    }

    public GameObject unloadBox(){
        if (numOfMeals > 0) {
            numOfMeals--;
            return boxPrefab; 
        }
        return null;
    }

    public int numOfBoxesInTruck(){
        return numOfMeals;
    }
}
}