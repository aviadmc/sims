using UnityEngine;
using System.Collections.Generic;
using Infra.Utils;

namespace Army {
public enum Item {
    None,
    Jerican,
}

/// <summary>
/// Hands that holds a tool and resources.
/// </summary>
public class Hands : MonoBehaviour {
    public GameObject holdedItem;

    protected void Awake() {
        holdedItem = null;
    }
}
}
