using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace Army {
public enum PointOfInterestType {
    None,
    Tent,
    ShootingTargets,
    Faucet,
    Jerican,
    Table,
    Truck,
    OutPlace,
}

public class PointOfInterest : MonoBehaviour, IStateful {
    public PointOfInterestType type;

    protected readonly State worldData = new State();
    protected bool hasJerican = false;
    protected bool jericanFull = false;

    protected virtual void Awake() {
        worldData["WithEmptyJerican"] = new StateValue(false);
        worldData["WithFullJerican"] = new StateValue(false);
    }

    /// <summary>
    /// Default implementation returns no state.
    /// </summary>
    public virtual State GetState() {
        worldData["WithEmptyJerican"].value = hasJerican && !jericanFull;
        worldData["WithFullJerican"].value = hasJerican && jericanFull;

        return worldData;
    }

    public void putFullJerican() {
        hasJerican = true;
        jericanFull = true;
    }

    public void putEmptyJerican() {
        hasJerican = true;
        jericanFull = false;
    }

    /// <summary>
    /// Returns all the points of interest of a given type.
    /// This is expensive so better cache the result on Start.
    /// </summary>
    public static List<IStateful> GetPointOfInterest(PointOfInterestType type) {
        var targets = GoapAction.GetTargets<PointOfInterest>();
        for (int i = targets.Count - 1; i >= 0; i--) {
            var point = targets[i] as PointOfInterest;
            if (point.type != type) {
                targets.RemoveAt(i);
            }
        }
        return targets;
    }
}
}