﻿using Ai.Goap;

namespace TeamFirewood
{
    public class Ruiner : Worker
    {
        private readonly WorldGoal worldGoal = new WorldGoal();
        private bool isHappy = true;

        protected override void Awake()
        {
            base.Awake();
            var goal = new Goal();
            goal["findBirds"] = new Condition(CompareType.Equal, true);
            worldGoal[this] = goal;
        }

        public override WorldGoal CreateGoalState()
        {
            isHappy = !isHappy;
            if (isHappy) {
                var goal = new Goal();
                goal["findBirds"] = new Condition(CompareType.Equal, true);
                worldGoal[this] = goal;
            }
            else {
                var goal = new Goal();
                goal["haveFun"] = new Condition(CompareType.Equal, true);
                worldGoal[this] = goal;
            }
            return worldGoal;
        }
    }
}
