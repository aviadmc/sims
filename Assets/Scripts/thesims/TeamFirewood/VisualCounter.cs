﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TeamFirewood
{
public class VisualCounter : MonoBehaviour {
    public Item type;
    
    private Container[] containers;
    private Text text;

    // Use this for initialization
    void Start () {
		GameObject piles = GameObject.Find("SupplyPiles");
        containers = piles.GetComponentsInChildren<Container>();
        text = GetComponent<Text>();
    }
	
    // Update is called once per frame
    void Update () {
		int counter = 0;
        foreach (Container child in containers){
            counter += child.items[type];
        }

        text.text = type.ToString() + " " + counter;
    }
}
}