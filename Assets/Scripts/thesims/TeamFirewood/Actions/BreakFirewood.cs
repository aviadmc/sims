﻿using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace TeamFirewood{
public class BreakFirewood : GoapAction{
    private List<IStateful> targets;

    protected void Awake(){
        AddPrecondition("hasTool", CompareType.Equal, true);
        AddEffect("haveFun", ModificationType.Set, true);

        AddTargetPrecondition(Item.Firewood.ToString(), CompareType.MoreThan, 0);
        AddTargetEffect(Item.Firewood.ToString(), ModificationType.Add, -1);
    }

    protected void Start(){
        targets = GetTargets<HarvestPoint>();
    }

    public override bool RequiresInRange(){
        return true;
    }

    public override List<IStateful> GetAllTargets(GoapAgent agent){
        return targets;
    }

    protected override bool OnDone(GoapAgent agent, WithContext context){
        var target = context.target as Component;
        var supplyPile = target.GetComponent<Container>();
        if (supplyPile.items[Item.Firewood] == 0)
        {
            return false;
        }
        supplyPile.items[Item.Firewood] -= 1;

        return base.OnDone(agent, context);
    }
}
}
