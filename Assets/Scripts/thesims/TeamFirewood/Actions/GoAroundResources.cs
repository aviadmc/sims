﻿using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace TeamFirewood{
public class GoAroundResources : GoapAction{
    private List<IStateful> targets;

    protected void Awake(){
        AddPrecondition("hasTool", CompareType.Equal, false);
        AddPrecondition("visit" + PointOfInterestType.ChoppingBlock, CompareType.Equal, true);
        AddPrecondition("visit" + PointOfInterestType.Forge, CompareType.Equal, true);
        AddPrecondition("visit" + PointOfInterestType.IronRock, CompareType.Equal, true);
        
        AddEffect("haveFun", ModificationType.Set, true);
        AddEffect("visit" + PointOfInterestType.ChoppingBlock, ModificationType.Set, false);
        AddEffect("visit" + PointOfInterestType.Forge, ModificationType.Set, false);
        AddEffect("visit" + PointOfInterestType.IronRock, ModificationType.Set, false);
    }

    protected void Start(){
        targets = GetTargets<HarvestPoint>();
    }

    public override bool RequiresInRange(){
        return false;
    }

    public override List<IStateful> GetAllTargets(GoapAgent agent){
        return targets;
    }

    protected override bool OnDone(GoapAgent agent, WithContext context){
        return base.OnDone(agent, context);
    }
}
}
