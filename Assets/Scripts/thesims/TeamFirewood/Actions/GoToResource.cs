﻿using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace TeamFirewood{
public class GoToResource : GoapAction{
    public PointOfInterestType craftingStation;
    private List<IStateful> targets;

    protected void Awake(){
        AddPrecondition("hasTool", CompareType.Equal, false);
        AddEffect("visit" + craftingStation, ModificationType.Set, true);
    }

    protected void Start(){
        targets = PointOfInterest.GetPointOfInterest(craftingStation);
    }

    public override bool RequiresInRange(){
        return true;
    }

    public override List<IStateful> GetAllTargets(GoapAgent agent){
        return targets;
    }

    protected override bool OnDone(GoapAgent agent, WithContext context){
        return base.OnDone(agent, context);
    }
}
}
