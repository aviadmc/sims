﻿using UnityEngine;
using System.Collections.Generic;
using Ai.Goap;

namespace TeamFirewood
{
    public class PutDownTool : GoapAction
    {
        public PointOfInterestType harvestSource;
        private List<IStateful> targets;

        protected void Awake()
        {
            AddPrecondition("hasTool", CompareType.Equal, true);
            AddEffect("hasTool", ModificationType.Set, false);
            AddTargetEffect(Item.NewTool.ToString(), ModificationType.Add, 1);
        }

        protected void Start()
        {
            targets = PointOfInterest.GetPointOfInterest(harvestSource);
        }

        public override bool RequiresInRange()
        {
            return true;
        }

        public override List<IStateful> GetAllTargets(GoapAgent agent)
        {
            return targets;
        }

        protected override bool OnDone(GoapAgent agent, WithContext context)
        {
            var target = context.target as Component;
            var supplyPile = target.GetComponent<Container>();
            var backpack = agent.GetComponent<Container>();
            
            supplyPile.items[Item.NewTool] += 1;

            backpack.tool.SetActive(false);
            backpack.tool = null;

            return base.OnDone(agent, context);
        }
    }
}
